﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RabbitMQ.Client;
using System.Text;
using System.Diagnostics;
using DVD_App_Sender.Models;

namespace DVD_App_Sender.Services
{
    public class EmitterService
    {
        public static void EmitDVD(DVD dvd)
        {
            var factory = new ConnectionFactory()
            {
                HostName = "localhost"
            };
            using(var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare("DVDs", "fanout");
               
                var message = BuildMessage(dvd);
                Debug.WriteLine("Message written " + message);
                var body = Encoding.UTF8.GetBytes(message);
                channel.BasicPublish(
                    exchange: "DVDs",
                    routingKey:"",
                    basicProperties:null,
                    body:body);
            }
        }

        private static string BuildMessage(DVD dvd)
        {
            return String.Format("DVD info:\n\t1.Title - {0}\n\t2.Year - {1}\n\t3.Price - {2}", dvd.Title, dvd.Year, dvd.Price); ;
        }
    }
}
