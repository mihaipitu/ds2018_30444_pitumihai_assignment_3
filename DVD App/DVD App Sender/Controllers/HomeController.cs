﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DVD_App_Sender.Models;
using DVD_App_Sender.Services;


namespace DVD_App_Sender.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            DVD dvd = new DVD();
            return View();
        }

        public IActionResult InsertDVD(DVD dvd)
        {
            if(ModelState.IsValid)
            {
                EmitterService.EmitDVD(dvd);
            }
            return RedirectToAction("Index");
        }
    }
}
