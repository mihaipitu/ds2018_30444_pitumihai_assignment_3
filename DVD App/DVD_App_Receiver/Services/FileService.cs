﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DVD_App_Receiver.Services
{
    public class FileService
    {
        public static void CreateDVDFile(string text)
        {
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            string folderName = Path.Combine(projectPath,"DVDs");

            if (!Directory.Exists(folderName))
            {
                Directory.CreateDirectory(folderName);
            }

            string file = Path.Combine(folderName, String.Format("DVD.txt"));
            if(!File.Exists(file))
            {
                File.Create(file);
            }
            File.WriteAllText(file, text);
            Console.WriteLine("File Created");
        }
    }
}
