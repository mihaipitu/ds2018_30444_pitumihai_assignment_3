﻿using DVD_App_EMailReceiver.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace DVD_App_EMailReceiver
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                Console.WriteLine("Connection has started.");

                channel.ExchangeDeclare("DVDs", "fanout");
                var queueName = channel.QueueDeclare().QueueName;
                channel.QueueBind(queueName, "DVDs", "");

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    Console.WriteLine("Package received.");
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine("I got this: " + message);
                    MailService.SendMessage(message, "mihai.pitu13@gmail.com");
                };
                channel.BasicConsume(queue: queueName, autoAck: true, consumer: consumer);
                Console.ReadLine();
            }
        }
    }
}
